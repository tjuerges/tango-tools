#! /usr/bin/env bash
#---------------------------------------------------------
#	Check $DISPLAY variable
#---------------------------------------------------------
if [ $DISPLAY ]
then
	echo "Display is $DISPLAY"
else
	echo ""
	echo "DISPLAY environment variable is not defined !"
	echo "Please, enter your DISPLAY name."
	read answer
	if [ $answer ]
	then
#		get the end of string to know if ":0" has been set
		start=`expr $answer : '.*:' \| $answer`
		echo "$start "
		if [ $start = $answer ]
		then
			DISPLAY=$answer:0
		else
			DISPLAY=$answer
		fi
		export DISPLAY
		echo "Starting astor on $DISPLAY"
	else
		echo "DISPLAY is not defined ! Astor cannot start !"
		exit
	fi
fi

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}

#---------------------------------------------------------
#	Add Doc path to $PATH (depends on OS used)
#---------------------------------------------------------
export TANGO_HOME=${MY_PATH}
MY_OS=`uname`
export MY_OS

CPP_DOC=NOT_INSTALLED
#---------------------------------------------------------
#	Set the template path
#---------------------------------------------------------
TEMPLATES=${MY_PATH}/templates
export TEMPLATES

#---------------------------------------------------------
#	Set the Home Source Path
#---------------------------------------------------------
SRC_PATH=${MY_PATH}/generated
export SRC_PATH

#---------------------------------------------------------
#	Set the Class Path for Tango and pogo usage
#---------------------------------------------------------
APP_DIR=${MY_PATH}/java			export APP_DIR
PREF_DIR=${MY_PATH}/preferences;   export PREF_DIR

POGO_CLASS=$APP_DIR/Pogo.jar;		export POGO_CLASS

CLASSPATH=$PREF_DIR:$POGO_CLASS;    export CLASSPATH

LOGBACK=${TANGO_LOGBACK:-${MY_PATH}/java/logback.xml}

#---------------------------------------------------------
#	Start the Pogo process
#---------------------------------------------------------
echo "Starting Pogo Appli under $MY_OS.  "
#

JAVA=java
[[ -f /usr/local/opt/openjdk@17/bin/java ]] && JAVA=/usr/local/opt/openjdk@17/bin/java
[[ -f /opt/homebrew/opt/openjdk@17/bin/java ]] && JAVA=/opt/homebrew/opt/openjdk@17/bin/java
${JAVA} \
 -DTEMPL_HOME=$TEMPLATES		\
		-DCPP_DOC_PATH=$CPP_DOC		\
		-DIN_LANG=$POGO_LANG		\
		-DEDITOR=$POGO_EDITOR 		\
		-Dfile.encoding=ISO-8859-1 	\
		-Dlogback.configurationFile="$LOGBACK" \
		org.tango.pogo.gui.Pogo \
		"$@"
